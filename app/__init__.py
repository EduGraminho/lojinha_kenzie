from flask import Flask
from app.views.status import bp_status
from app.views.users import bp_users
from app.models import db
from environs import Env

def create_app():
    env = Env()
    env.read_env()
        
    app = Flask(__name__)
    
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = env.bool('SQLALCHEMY_TRACK_MODIFICATIONS')
    app.config['SQLALCHEMY_DATABASE_URI'] = env.str('SQLALCHEMY_DATABASE_URI')
    
    db.init_app(app)
    
    app.register_blueprint(bp_status)
    app.register_blueprint(bp_users)

    return app
